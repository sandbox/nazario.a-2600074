<?php
/**
 * @file
 * Generates an age verification screen before admitting user to the site.
 *
 * If the user validates their age either by supplying a birthday or affirming
 * that they are of a legal age then the age gate will deposit a cookie in the
 * browser and allow the user to continue onto the site.
 *
 * This was developed by Nazario A. Ayala (Niztech) On Febuary 14th, 2013 and
 * last updated in 2016.
 */

/**
 * Constants.
 *
 * These constants are here to make the code easier to read and communicate
 * settings to the JS file.
 */
define('AGEGATE_SHOW', 1);
define('AGEGATE_HIDE', 0);

define('AGEGATE_DECODED', 1);
define('AGEGATE_ENCODED', 0);

define('AGEGATE_YES', 1);
define('AGEGATE_NO', 0);

/**
 * Implements hook_preprocess_page().
 *
 * This function will be called for every page load.
 * On every page load this module will check to see
 * if the agegate should be displayed.
 */
function agegate_preprocess_page(&$vars) {

  // Going through the module configuration settings to see if the agegate
  // should be or not displayed on the current page.
  //
  // Setting initial flags.
  $display = AGEGATE_YES;
  $environment = 'live';

  /*
   * If this module is running on a PANTHEON server, PANTHEON specific settings
   * can be set. If the module is not runnining on a PANTHEON server a generic
   * setting is used.
   */
  if (isset($_SERVER['PANTHEON_ENVIRONMENT'])) {
    $environment = $_SERVER['PANTHEON_ENVIRONMENT'];

    switch ($environment) {
      case 'dev':
        if (variable_get('agegate_show') == AGEGATE_HIDE) {
          $display = AGEGATE_NO;
        }
        break;

      case 'test':
        if (variable_get('agegate_show') == AGEGATE_HIDE) {
          $display = AGEGATE_NO;
        }
        break;

      case 'live':
        $display = AGEGATE_YES;
        break;
    }
  }
  else {
    if (variable_get('agegate_show') == AGEGATE_HIDE) {
      $display = AGEGATE_NO;
    }
  }

  if (user_access('administrator') == AGEGATE_YES) {
    $display = AGEGATE_NO;
  }

  // Is the current page one of the pages where the Agegate not be dispalyed?
  if (drupal_match_path(request_path(), variable_get('agegate_hidefrom'))) {
    $display = AGEGATE_NO;
  }

  $settings = array(
    'cookiedomain'    => variable_get('agegate_cookiedomain'),
    'cookiename'      => variable_get('agegate_cookiename'),
    'cookieformat'    => variable_get('agegate_cookieformat'),
    'cancelurl'       => variable_get('agegate_cancelurl'),
    'verificationtype' => variable_get('agegate_verificationtype'),
    'displaypopup'    => $display,
    'legalage'        => variable_get('agegate_legalage'),
  );

  $settings['popuphtml'] = theme('agegate_popup', array(
    'agegate_html_validatebox'  => agegate_verification_box_html($settings['verificationtype'], array('age' => $settings['legalage'])),
    'agegate_name'          => check_plain(check_plain(variable_get('agegate_sitename'))),
    'agegate_message'       => check_plain(variable_get('agegate_sitemessage')),
    'agegate_html_btn_cancel' => '<button id="agegate_cancel" class="button cancel-verify">' . check_plain(variable_get('agegate_cancelbtntext')) . '</button>',
    'agegate_html_btn_verify' => '<button id="agegate_verify" class="button age-verify">' . check_plain(variable_get('agegate_verifybtntext')) . '</button>',
  ));

  drupal_add_js(array('agegate' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'agegate') . '/resources/agegate.js', 'file');
}

/**
 * Implements hook_help().
 *
 * This is supposed to appear in the Help menu
 * There should also be a link in the module page.
 */
function agegate_help($path, $arg) {
  switch ($path) {
    case 'admin/help#agegate':
      $output = '<h3>' . t('About') . '</h3>';
      $output += '<p>' . t('This module creates an agegate. Visitors to the site must confirm their age as being over 21 in order to proceed visiting the site.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function agegate_permission() {
  return array(
    'admin agegate settings' => array(
      'title' => t('Admminister Agegate'),
      'description' => t('Configure Agegate Options'),
    ),
  );
}

/**
 * Imeplements hook_menu().
 */
function agegate_menu() {
  $items = array();

  // Admin Configuration Group
  // When an admin has logged in -- this creates a group
  // under the configuration page.
  $items['admin/config/agegate'] = array(
    'title' => 'Agegate',
    'description' => 'Administer Agegate Options',
    'access arguments' => array('admin agegate settings'),
  );

  // Admin configuration - Settings
  // Adds a link under the Admin Configuration Group
  // mentioned in the above block.
  $items['admin/config/agegate/manage'] = array(
    'title' => 'Agegate Settings',
    'description' => 'Manage Agegate Cart Options and Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('agegate_admin_settings_form'),
    'access arguments' => array('admin agegate settings'),
  );

  return $items;
}


/**
 * Implements hook_form().
 */
function agegate_admin_settings_form($node, &$form_state) {
  $form = array();
  $form['overview'] = array(
    '#markup' => t('This interface allows administrators to manage settings for the Agegate.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  // GENERAL.
  $form['general'] = array(
    '#title' => t('General Settings'),
    '#description' => t("Note that you have greater control over markup if you use the <strong>agegate--popup.tpl.php</strong> file found in the module's code. Make a copy of the TPL file and place it in your theme's folder."),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $form['general']['agegate_sitename'] = array(
    '#title' => t('Website Name'),
    '#description' => t('How would you like your websites name to display on the agegate.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_sitename', variable_get('site_name')),
  );
  $form['general']['agegate_sitemessage'] = array(
    '#title' => t('Legal Message'),
    '#description' => t('Set the legal verbage that the user sees.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('agegate_sitemessage', t('By clicking SUBMIT you verify that you are 21 years of age or older.')),
  );
  $form['general']['agegate_verifybtntext'] = array(
    '#title' => t('Verify Button Text'),
    '#description' => t('Text of the verify button.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_verifybtntext', t('Verify')),
  );
  $form['general']['agegate_cancelbtntext'] = array(
    '#title' => t('Cancel Button Text'),
    '#description' => t('Text of the cancel button.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_cancelbtntext', t('Cancel')),
  );
  $form['general']['agegate_cancelurl'] = array(
    '#title' => t('Cancel URL'),
    '#description' => t('Cancel URL to redirect a user who has not verified their age.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_cancelurl', 'http://www.centurycouncil.org/'),
  );
  $form['general']['agegate_verificationtype'] = array(
    '#title' => t('Verification Type'),
    '#description' => t('How should the visitor verify their age?'),
    '#type' => 'select',
    '#default_value' => variable_get('agegate_verificationtype'),
    '#options' => array(
      1 => t('Prompt user for birthday'),
      2 => t('Prompt user with a yes / no question'),
    ),
  );
  $form['general']['agegate_legalage'] = array(
    '#title' => t('Legal Age'),
    '#description' => t('The legal age to check against.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_legalage', 21),
  );

  // Environment Settings.
  $form['environmentsettings'] = array(
    '#title' => t('environment Settings'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $hidefrom_defaults = array(
    'contact-us',
    'privacy-policy',
    'terms-and-conditions',
    'node/example',
  );
  $form['environmentsettings']['agegate_hidefrom'] = array(
    '#title' => t('Do Not Display Agegate On These Pages'),
    '#description' => t('What pages would you like to exclude the Agegate from? Do not include website domain or trailing slash.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('agegate_hidefrom', implode("\n", $hidefrom_defaults)),
  );

  if (isset($_SERVER['PANTHEON_ENVIRONMENT'])) {
    $environment = $_SERVER['PANTHEON_ENVIRONMENT'];

    switch ($environment) {
      case 'dev':
        $form['environmentsettings']['agegate_show'] = array(
          '#title' => t('Show On Dev'),
          '#type' => 'checkbox',
          '#default_value' => variable_get('agegate_show', AGEGATE_HIDE),
        );
        $form['environmentsettings']['agegate_cookiedomain'] = array(
          '#title' => t("Cookie Domain"),
          '#description' => t('Set the DEV domain name that eWinery DEV will use. It is recommended that it be a top level domain.<br />Note: you need not use eWinery in order to use this module.'),
          '#type' => 'textfield',
          '#default_value' => variable_get('agegate_cookiedomain', '.domain.com'),
        );
        break;

      case 'test':
        $form['environmentsettings']['agegate_show'] = array(
          '#title' => t('Show On Test'),
          '#type' => 'checkbox',
          '#default_value' => variable_get('agegate_show', AGEGATE_SHOW),
        );
        $form['environmentsettings']['agegate_cookiedomain'] = array(
          '#title' => t("Cookie Domain"),
          '#description' => t('Set the TEST domain name that eWinery TEST will use. It is recommended that it be a top level domain.<br />Note: you need not use eWinery in order to use this module.'),
          '#type' => 'textfield',
          '#default_value' => variable_get('agegate_cookiedomain', '.domain.com'),
        );
        break;

      case 'live':
        $form['environmentsettings']['agegate_show'] = array(
          '#title' => t('Show On Live'),
          '#type' => 'checkbox',
          '#default_value' => variable_get('agegate_show', AGEGATE_SHOW),
          '#disabled' => TRUE,
        );
        $form['environmentsettings']['agegate_cookiedomain'] = array(
          '#title' => t("Cookie Domain"),
          '#description' => t('Set the LIVE domain name that eWinery LIVE will use. It is recommended that it be a top level domain.<br />Note: you need not use eWinery in order to use this module.'),
          '#type' => 'textfield',
          '#default_value' => variable_get('agegate_cookiedomain', '.domain.com'),
        );
        break;
    }
  }
  else {
    $form['environmentsettings']['agegate_show'] = array(
      '#title' => t('Show On Live'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('agegate_show', AGEGATE_SHOW),
    );
    $form['environmentsettings']['agegate_cookiedomain'] = array(
      '#title' => t("Cookie Domain"),
      '#description' => t('Set the LIVE domain name that eWinery LIVE will use. It is recommended that it be a top level domain.<br />Note: you need not use eWinery in order to use this module.'),
      '#type' => 'textfield',
      '#default_value' => variable_get('agegate_cookiedomain', '.domain.com'),
    );
  }

  // ADVANCED.
  $form['advanced'] = array(
    '#title' => t('Advanced'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['agegate_cookiename'] = array(
    '#title' => t('Cookie Name'),
    '#description' => t("Set the cookie name. Ewinery looks for a cookie named 'ISLEGAL'. You probobly don't need to change this."),
    '#type' => 'textfield',
    '#default_value' => variable_get('agegate_cookiename', 'ISLEGAL'),
  );
  $form['advanced']['agegate_cookieformat'] = array(
    '#title' => t('Cookie format encoded'),
    '#description' => t('eWinery requires that the cookie be stored in an encoded format. If for some reason you need the cookie stored in a raw format (decoded) un-check this box.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('agegate_cookieformat', AGEGATE_ENCODED),
  );

  return system_settings_form($form);
}


/**
 * Implements hook_validate().
 */
function agegate_admin_settings_form_validate($form, &$form_state) {
  // Check that the domain is valid -- we have a very loose definition
  // for a valid cookie domain name.
  //
  // A valid cookie domain consist of a string starting with a period "." and
  // having at least one more period after it and having as its only other
  // characters "a" through "z", "A" through "Z", and underscore "_" and a
  // hyphon "-".
  //
  // Example of Valid:
  // .domain.com
  // .domain_with.crazy-extension
  // .local.dev.domain.com
  //
  // Examples of Inavlid:
  // .com (must have two periods).
  // test.domain.com (must start with a period).
  // .my domain.com (conatins illegal character).
  $cookie_domain = $form_state['values']['agegate_cookiedomain'];
  $grep = '/(^\.[A-Za-z0-9\-\_]+)(\.[A-Za-z0-9\-\_]+)+$/';
  if (preg_match($grep, $cookie_domain) == AGEGATE_NO) {
    form_set_error('agegate_cookiedomain', t('You have added an invalid domain name.'));
  }

  // Check that the legal age input is valid.
  // Must only contain digits.
  $legal_age = $form_state['values']['agegate_legalage'];
  $grep = '/^[0-9]+$/';
  if (preg_match($grep, $legal_age) == AGEGATE_NO) {
    form_set_error('agegate_legalage', t('The legal drinking age must be a positive integer.'));
  }
}


/**
 * Implements hook_theme().
 */
function agegate_theme($existing, $type, $theme, $path) {
  $result = array(
    'agegate_popup' => array(
      'template' => 'agegate--popup',
      'variables' => array(),
    ),
  );
  return $result;
}


/**
 * Generates HTML for the verification box.
 *
 * This is based on the what the administrator has set for verification type.
 *
 * @param int $verificationtype - An integer that is used to represent the
 * verification type that the has been configured to be used.
 *
 * @param array $options - an array of options that may be needed for UI/UX
 * purposes. Typically $options['age'] is used as the defined "valid age."
 *
 * @return string || false - Returns an HTML string which is the code needed for
 * to render the input for that verification type.
 */
function agegate_verification_box_html($verificationtype, $options) {
  if ($verificationtype == 1) {
    $maxdate = $options['age'] - 1;

    drupal_add_library('system', 'ui.datepicker');
    drupal_add_js('jQuery(document).ready(function() {jQuery("#agegate_birthday").datepicker({dateFormat: "mm/dd/yy", autoSize: true, changeYear: true, maxDate: "-' . $maxdate . 'y"}); });', 'inline');
    return '<div>' . t('Select Birthday') . ':<br /><input type="text" class="datepicker agegate-error" id="agegate_birthday" value="Date"/></div>';
  }
  elseif ($verificationtype == 2) {
    return '<input type="hidden" value="' . $options['age'] . '" id="agegate_birthday">';
  }
  else {
    return FALSE;
  }
}
